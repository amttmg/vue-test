import Vue from 'vue'
import App from './App.vue'
import {firestorePlugin} from 'vuefire'
import {BootstrapVue, IconsPlugin} from 'bootstrap-vue'
import Toaster from 'v-toaster'
import 'v-toaster/dist/v-toaster.css'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'handsontable/dist/handsontable.full.css'
import VueSplit from 'vue-split-panel'



Vue.config.productionTip = false
Vue.use(Toaster, {timeout: 5000})
Vue.use(firestorePlugin);
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(VueSplit)

new Vue({
  render: h => h(App),
}).$mount('#app')
