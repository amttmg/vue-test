import firebase from 'firebase'
import 'firebase/firestore'

let firebaseConfig = {
  apiKey: "AIzaSyBpWoaMuAhyVEpCTdsmbfhjbyhxpoPYA1w",
  authDomain: "vue-firebase-test-89199.firebaseapp.com",
  databaseURL: "https://vue-firebase-test-89199.firebaseio.com",
  projectId: "vue-firebase-test-89199",
  storageBucket: "vue-firebase-test-89199.appspot.com",
  messagingSenderId: "918863923787",
  appId: "1:918863923787:web:6010adc538c8f0f61d9bce"
}
// Initialize Firebase
export const db = firebase.initializeApp(firebaseConfig).firestore();
